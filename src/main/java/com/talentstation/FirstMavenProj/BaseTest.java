package com.talentstation.FirstMavenProj;

import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.util.Properties;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class BaseTest {
	WebDriver driver;
	public ExtentTest logger;
	Properties prop;
	FileInputStream fs;
	
	public void TestDataSetup(){
		
		prop = new Properties();
		fs = new FileInputStream(System.getProperty(("user.dir")+"/application.properties"));
		prop.load(fs);
		
		System.out.println(System.getProperty("browser"));
		System.out.println(System.getProperty("url"));
	}
	
	
	
	@BeforeMethod()
	public void startBrowser(Method caller){
		
		TestDataSetup();
		
		 logger=ComplexReportFactory.getTest(caller.getName(), "This is a simple test from complex factory");
		 
		 System.out.println(System.getProperty("browser"));
			System.out.println(System.getProperty("url"));
		if((System.getProperty("browser").equals("chrome")))
				driver = new chromeDriver();
		else if((System.getProperty("browser").equals("firefox")))
			driver = new FirefoxeDriver();
		
		else
			System.out.println("driver not found");
	}
	
	@AfterMethod
    public void afterMethod(ITestResult result,Method caller) {

        if(ITestResult.FAILURE==result.getStatus()){
            String screenshotPath= Utility.captureScreenShot(driver, result.getName());
            String image=logger.addScreenCapture(screenshotPath);
            logger.log(LogStatus.FAIL, "Title verification", image);
        }
        
         //reports.endTest(logger);
         //reports.flush();
         driver.quit();
        ComplexReportFactory.closeTest(caller.getName());

    }

    /*
     * After suite will be responsible to close the report properly at the end
     * You an have another afterSuite as well in the derived class and this one
     * will be called in the end making it the last method to be called in test exe
     */
	@AfterSuite
    public void afterSuite() {
        ComplexReportFactory.closeReport();
    }

}